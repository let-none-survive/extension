let params = {
  active: true,
  currentWindow: true
}
$('#select').on('change', function() {
  let fontName = $(this).children(":selected").attr("name");
  function sendMsg(tabs) {
    let msg = fontName;
    chrome.tabs.sendMessage(tabs[0].id, msg)
  }
  chrome.tabs.query(params, sendMsg);

  });
